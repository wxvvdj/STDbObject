//
//  main.m
//  STDbKit
//
//  Created by yls on 10/17/2014.
//  Copyright (c) 2014 yls. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "STAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([STAppDelegate class]));
    }
}
