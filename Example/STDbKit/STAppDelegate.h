//
//  STAppDelegate.h
//  STDbKit
//
//  Created by CocoaPods on 10/17/2014.
//  Copyright (c) 2014 yls. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface STAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
