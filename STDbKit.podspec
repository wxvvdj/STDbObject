#
# Be sure to run `pod lib lint STDbKit.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "STDbKit"
  s.version          = "1.0"
  s.summary          = "like CoreData, object auto convert to sqlite3."
  s.description      = 'this provide class of STDbObject which you can use conveniently to insert, select, update, delete.'
  s.homepage         = "https://git.oschina.net/yanglishuan/STDbObject"
  s.license          = 'MIT'
  s.author           = { "yls" => "2008.yls@163.com" }
  s.source           = { :git => "https://git.oschina.net/yanglishuan/STDbObject.git", :tag => '1.0' }
  s.platform     = :ios, '8.0'
  s.requires_arc = true
  s.source_files = 'Pod/Classes'
  s.public_header_files = 'Pod/Classes/Public/**/*.h'
end
